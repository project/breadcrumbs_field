<?php

declare(strict_types=1);

namespace Drupal\breadcrumbs_field\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\StringItem;
use PhpParser\Node\Expr\ArrayItem;

/**
 * Breadcrumb link item.
 *
 * @FieldType(
 *   id = "breadcrumbs_json_ld",
 *   label = @Translation("Breadcrumbs JSON-LD"),
 *   description = @Translation("Breadcrumb links associated with the entity in
 *   Json-LD format."),
 *   default_widget = "string_textfield",
 *   default_formatter = "breadcrumbs_json_ld"
 * )
 */
final class BreadcrumbsJsonLdItem extends StringItem {

  public const BREADCRUMBS_JSON_LD_FIELD_TYPE = 'breadcrumbs_json_ld';

}

