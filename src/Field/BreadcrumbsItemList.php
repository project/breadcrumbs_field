<?php

namespace Drupal\breadcrumbs_field\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Provides a computed breadcrumbs field item list.
 */
class BreadcrumbsItemList extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $parent */
    $parent = $this->getEntity();
    if ($parent->isNew()) {
      return;
    }
    /** @var \Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface $breadcrumbsService */
    $breadcrumbsService = \Drupal::service('breadcrumb');
    $url = $parent->toUrl();
    /** @var \Drupal\Core\Routing\RouteProviderInterface $routeProvider */
    $routeProvider = \Drupal::service('router.route_provider');
    $routeMatch = new RouteMatch(
      $url->getRouteName(),
      $routeProvider->getRouteByName($url->getRouteName()),
      [
        $parent->getEntityTypeId() => $parent,
      ],
      $url->getRouteParameters()
    );
    $breadcrumb = $breadcrumbsService->build($routeMatch);
    foreach ($breadcrumb->getLinks() as $delta => $item) {
      $linkUrl = $item->getUrl()->toString();
      $this->list[] = $this->createItem(
        $delta,
        [
          'uri' => empty($linkUrl) ? 'internal:#' : $linkUrl,
          'title' => $item->getText(),
        ]
      );
    }
  }

}
