<?php

declare(strict_types=1);

namespace Drupal\breadcrumbs_field\Field;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\TypedData\ComputedItemListTrait;
use function assert;
use Drupal;

/**
 * Provides a computed breadcrumbs field item list.
 */
final class BreadcrumbsJsonLdItemList extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritDoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  protected function computeValue() {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $parent */
    $parent = $this->getEntity();
    if ($parent->isNew()) {
      return;
    }
    /** @var \Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface $breadcrumbsService */
    $breadcrumbsService = \Drupal::service('breadcrumb');
    $url = $parent->toUrl();
    /** @var \Drupal\Core\Routing\RouteProviderInterface $routeProvider */
    $routeProvider = \Drupal::service('router.route_provider');
    $routeMatch = new RouteMatch(
      $url->getRouteName(),
      $routeProvider->getRouteByName($url->getRouteName()),
      [
        $parent->getEntityTypeId() => $parent,
      ],
      $url->getRouteParameters()
    );
    $breadcrumb = $breadcrumbsService->build($routeMatch);
    $value = [
      '@context' => "https://schema.org",
      '@type' => "BreadcrumbList",
      'itemListElement' => [],
    ];

    foreach ($breadcrumb->getLinks() as $delta => $item) {
      $linkUrl = $item->getUrl()->toString();
      $name = $item->getText();
      $value['itemListElement'][$delta] = [
        '@type' => 'ListItem',
        'position' => $delta + 1,
        'name' => $name,
      ];
      if (!empty($linkUrl)) {
        $value['itemListElement'][$delta]['item'] = $linkUrl;
      }
    }
    $this->list[] = $this->createItem(0, json_encode($value, JSON_THROW_ON_ERROR));
  }

}
