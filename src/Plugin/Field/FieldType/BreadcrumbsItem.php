<?php

namespace Drupal\breadcrumbs_field\Plugin\Field\FieldType;

use Drupal\link\Plugin\Field\FieldType\LinkItem;

/**
 * Breadcrumb link item.
 *
 * @FieldType(
 *   id = "breadcrumbs",
 *   label = @Translation("Breadcrumbs"),
 *   description = @Translation("Breadcrumb links associated with the entity."),
 *   default_widget = "link_default",
 *   default_formatter = "link",
 *   constraints = {
 *     "LinkType" = {},
 *     "LinkAccess" = {},
 *     "LinkExternalProtocols" = {},
 *      "LinkNotExistingInternal" = {}
 *   }
 * )
 */
class BreadcrumbsItem extends LinkItem {

  public const BREADCRUMBS_LINK_FIELD_TYPE = 'breadcrumbs';

}
