<?php declare(strict_types = 1);

namespace Drupal\breadcrumbs_field\Form;

use Drupal\Core\Config\TypedConfigManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure breadcrumbs_field settings.
 */
final class SettingsForm extends ConfigFormBase {
  private EntityTypeManagerInterface $entityTypeManager;

  public function __construct(EntityTypeManagerInterface $entityTypeManager, ConfigFactoryInterface $config_factory, $typedConfigManager = NULL)
  {
    parent::__construct($config_factory, $typedConfigManager);
    $this->entityTypeManager = $entityTypeManager;
  }

  public static function create(ContainerInterface $container): ConfigFormBase|SettingsForm
  {
    /** @var EntityTypeManagerInterface $entityTypeManager */
    $entityTypeManager = $container->get('entity_type.manager');
    /** @var ConfigFactoryInterface $configFactory */
    $configFactory = $container->get('config.factory');
    /** @var TypedConfigManagerInterface|null $typeConfig */
    $typeConfig = $container->get('config.typed');
    return new self(
      $entityTypeManager,
      $configFactory,
      $typeConfig
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'breadcrumbs_field_settings';
  }

  /**
   * @return EntityTypeInterface[]
   */
  private function getAllowedEntityTypes(): array
  {
    $entity_types = $this->entityTypeManager->getDefinitions();
    return array_filter($entity_types, static function (EntityTypeInterface $entityType) {
      return $entityType->getGroup() === 'content'
        && $entityType->hasLinkTemplate('canonical');
    });
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['breadcrumbs_field.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['enable_json_ld'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable JSON-LD'),
      '#description' =>$this->t('If checked, and additional field containing breadcrumbs in JSON-LD format will be added to the entity.'),
      '#default_value' => $this->config('breadcrumbs_field.settings')->get('enable_json_ld'),
    ];

    $allowedEntityTypes = $this->getAllowedEntityTypes();
    $selectedEntityTypes = $this->config('breadcrumbs_field.settings')->get('allowed_entity_types');

    $form['allowed_entity_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Allowed entity types'),
      '#description' =>$this->t('Select which entity types you want to attach their breadcrumbs to'),
      '#options' => array_map(static fn(EntityTypeInterface $entityType) => $entityType->getLabel(), $allowedEntityTypes),
      '#default_value' => $selectedEntityTypes,
      '#size' => 6,
      '#multiple' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();
    $allowedEntityTypes = array_keys(array_filter($values['allowed_entity_types']));
    $this->config('breadcrumbs_field.settings')
      ->set('enable_json_ld', (bool) $values['enable_json_ld'])
      ->set('allowed_entity_types', $allowedEntityTypes)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
