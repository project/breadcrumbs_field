## INTRODUCTION

The Breadcrumbs Field module provides a computed field for entities to expose their breadcrumbs using core breadcrumbs service
The primary use case for this module is exposing breadcrumbs for entities to the JSON:API module.

### Differences with similar modules

- [Computed Breadcrumbs](https://dgo.to/computed_breadcrumbs): this module provides a similar feature but is based on the `menu.menu_trail` service instead of the `breadcrumb` service, the latter is more powerful because does not rely solely on the URL path.
- [Breadcrumb Field](https://dgo.to/breadcrumb_field): this module provides a field to customize each content breadcrumb but does not expose the default breadcrumb.

## REQUIREMENTS

None, just core. Since it relies on core breadcrumbs service is compatible with other modules like [Easy Breadcrumbs](https://dgo.to/easy_breadcrumbs) module.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
Visit [Drupal.org documentation](https://www.drupal.org/node/895232) for further information.

## CONFIGURATION

No configuration is required.

## MAINTAINERS

Current maintainers for Drupal 10:
- Dennis A. Torres (d70rr3s) - https://www.drupal.org/u/d70rr3s
